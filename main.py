import pgzrun
from math import pi, atan2, sin, cos, degrees, dist
from vector import Vector

WIDTH = 800
HEIGHT = 600


border = {
    'left': 100,
    'top': 100,
    'right': WIDTH-100,
    'bottom': HEIGHT-100,
    }




def keyboard_controls(heading, dt):
    if keyboard.left:
        return heading.turn(-pi/2*dt)
    if keyboard.right:
        return heading.turn(pi/2*dt)
    return heading

def draw():
    screen.fill((31,11,82))
    head.angle = -degrees(heading.heading())
    head.draw()
    for b in body:
        b.angle = -degrees((Vector(*b.behind.pos)-Vector(*b.pos)).heading())
        b.draw()
    for e in enemies:
        e.draw()

def move_head(head, heading, dt):
    new = Vector(*head.pos)+heading*dt
    if new.y>=border['bottom'] or new.y<=border['top']:
        heading = Vector(heading.x, -heading.y)
        new = Vector(*head.pos)+heading*dt
    if new.x>=border['right'] or new.x<=border['left']:
        heading = Vector(-heading.x, heading.y)
        new = Vector(*head.pos)+heading*dt
    return new.as_tuple(), heading

def update(dt):
    global heading, enemy_timer
    heading = keyboard_controls(heading, dt)
    head.pos, heading = move_head(head, heading, dt)
    for b in body:
        b.pos = move_body(b, dt)
    enemy_timer -= dt
    if enemy_timer<=0:
        enemies.append(Actor('enemy', center=(200, 200)))
        enemy_timer = 2

def move_body(b, dt):
    _heading = Vector(*b.behind.pos)    
    l = (Vector(*b.pos) - _heading).len()
    pos = Vector(*b.pos) + (_heading - Vector(*b.pos))*100/l*dt
    while (pos - _heading).len()<18:
        pos = pos - (_heading - pos)*100/l*0.001
    
    return pos.as_tuple()

enemy_timer = 2
enemies = []
heading = Vector(100, 0)
head = Actor('snake_body')
head.pos = spawn = 400, 300
last = head
i = 0
body = []
while i<7:
    update(0.016)
    if dist(last.pos, spawn)>16:
        new = Actor('snake_body', center=spawn)
        new.behind = last
        last = new
        body.append(last)
        i += 1

pgzrun.go()
