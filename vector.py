from math import hypot, atan2, cos, sin

class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __add__(self, other):
        return Vector(self.x+other.x, self.y+other.y)
    def __sub__(self, other):
        return Vector(self.x-other.x, self.y-other.y)
    def __mul__(self, k):
        return Vector(self.x*k, self.y*k)
    def __truediv__(self, k):
        return self*(1/k)
    def len(self):
        return hypot(self.x, self.y)
    def heading(self):
        return atan2(self.y, self.x)
    def as_tuple(self):
        return self.x, self.y
    def turn(self, angle):
        x, y = self.x, self.y
        a = atan2(y, x) + angle
        x = self.len()*cos(a)
        y = self.len()*sin(a)
        return Vector(x, y)
